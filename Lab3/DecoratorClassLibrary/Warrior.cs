﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorClassLibrary
{
    public class Warrior : Character
    {
        public Warrior(string name, Inventory inventory) : base(name, inventory)
        {
        }

        public override void Attack()
        {
            base.Attack();
            Console.WriteLine($"Charge into battle!\n");
        }
    }
}
