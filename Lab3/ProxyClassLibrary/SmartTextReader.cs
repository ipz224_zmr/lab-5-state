﻿namespace ProxyClassLibrary
{
    public class SmartTextReader : ITextReader
    {
        public char[][] ReadText(string filePath)
        {
            try
            {
                string[] lines = File.ReadAllLines(filePath);
                char[][] result = new char[lines.Length][];
                for (int i = 0; i < lines.Length; i++)
                {
                    result[i] = lines[i].ToCharArray();
                }
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error reading file: " + ex.Message);
                return null;
            }
        }
    }
}