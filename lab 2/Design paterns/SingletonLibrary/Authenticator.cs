﻿namespace SingletonLibrary
{
    public class Authenticator
    {
        private static Authenticator instance;
        private static readonly object lockObject = new object();

        protected Authenticator() { }

        public static Authenticator Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (lockObject)
                    {
                        if (instance == null)
                        {
                            instance = new Authenticator();
                        }
                    }
                }
                return instance;
            }
        }
    }
}