﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethodLibrary
{
    public class MobileApp : ISubscriptionFactory
    {
        public Subscription CreateSubscription()
        {
            return new EducationalSubscription(new List<string> { "Educational", "Documentary", "Science" });

        }
    }
}
