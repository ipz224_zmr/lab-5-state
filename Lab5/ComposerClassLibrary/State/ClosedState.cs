﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ComposerClassLibrary.State
{
    public class ClosedState : IElementState<LightElementNode>
    {
        public string Render(LightElementNode element)
        {
            string cssClassStr = string.Join(" ", element.CSSClasses);
            string attributes = $"class=\"{cssClassStr}\"";
            return $"</{element.TagName} {attributes}>";
        }
    }

}
