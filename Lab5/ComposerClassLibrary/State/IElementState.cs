﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComposerClassLibrary.State
{
    public interface IElementState<T>
    {
        string Render(T element);
    }
}
