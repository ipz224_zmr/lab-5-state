﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComposerClassLibrary.State
{
    public class OpenState : IElementState<LightElementNode>
    {
        public string Render(LightElementNode element)
        {
            string cssClassStr = string.Join(" ", element.CSSClasses);
            string attributes = $"class=\"{cssClassStr}\"";
            string childrenHTML = string.Join("", element.ChildNodes.ConvertAll(node => node.OuterHTML));

            return $"<{element.TagName} {attributes}>{childrenHTML}</{element.TagName}>";
        }
    }
}
