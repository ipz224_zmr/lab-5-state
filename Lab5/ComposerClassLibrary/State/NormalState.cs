﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComposerClassLibrary.State
{
    public class NormalState : IElementState<string>
    {
        public string Render(string text)
        {
            return text;
        }
    }
}
