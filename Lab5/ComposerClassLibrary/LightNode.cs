﻿using ComposerClassLibrary.State;

namespace ComposerClassLibrary
{
    public class LightNode
    {
        public virtual string OuterHTML { get; }
        public virtual string InnerHTML { get; }
    }
}