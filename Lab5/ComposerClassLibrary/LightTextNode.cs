﻿using ComposerClassLibrary.State;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComposerClassLibrary
{

    public class LightTextNode : LightNode
    {
        public string Text { get; }

        private readonly IElementState<string> _state;

        public LightTextNode(string text)
        {
            Text = text;
            _state = new NormalState(); 
        }

        public override string OuterHTML => _state.Render(Text);

        public override string InnerHTML => _state.Render(Text);
    }



}
