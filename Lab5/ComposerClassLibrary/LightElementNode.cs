﻿using ComposerClassLibrary.State;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComposerClassLibrary
{
    public class LightElementNode : LightNode
    {
        public string TagName { get; }
        public string DisplayType { get; }
        public List<string> CSSClasses { get; }
        public List<LightNode> ChildNodes { get; }
        private IElementState<LightElementNode> _state;
        public LightElementNode(string tagName, string displayType, List<string> cssClasses, List<LightNode> childNodes)
        {
            TagName = tagName;
            DisplayType = displayType;
            CSSClasses = cssClasses;
            ChildNodes = childNodes;
            _state = new OpenState();
        }

       
        public void ChangeState(IElementState<LightElementNode> state)
        {
            _state = state;
        }

        public override string OuterHTML => _state.Render(this);

        public override string InnerHTML => string.Join("", ChildNodes.ConvertAll(node => node.OuterHTML));
    }

}
