﻿
using ComposerClassLibrary;
using ComposerClassLibrary.State;

class Program
{
    static void Main(string[] args)
    {
        LightTextNode textNode = new LightTextNode("This is a text node.");
        LightElementNode elementNode = new LightElementNode("div", "block", new List<string> { "class1", "class2" }, new List<LightNode> { textNode });

        
        elementNode.ChangeState(new ClosedState());

        Console.WriteLine("Normal State:");
        Console.WriteLine(textNode.OuterHTML);

        Console.WriteLine("Close State:");
        Console.WriteLine("Outer HTML:");
        Console.WriteLine(elementNode.OuterHTML);

        Console.WriteLine("OpenState:");
        elementNode.ChangeState(new OpenState());
        Console.WriteLine("Outer HTML:");
        Console.WriteLine(elementNode.OuterHTML);
        Console.WriteLine("\nInner HTML:");
        Console.WriteLine(elementNode.InnerHTML);


    }
}