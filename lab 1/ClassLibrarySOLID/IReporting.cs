﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrarySOLID
{
    
    public interface IReporting
    {
        void RegisterIncome(WarehouseItem item);
        void RegisterOutcome(WarehouseItem item);
        void GenerateInventoryReport();
    }
}
