﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrarySOLID
{
    public class Warehouse
    {
        private readonly List<WarehouseItem> items;
        public List<WarehouseItem> Items
        {
            get { return items; }
        }
 
        public Warehouse(List<WarehouseItem> items)
        {
            this.items = items;
          
        }
        
        public void AddItem(WarehouseItem item)
        {
            items.Add(item);
        }

        public void RemoveItem(WarehouseItem item)
        {
            items.Remove(item);
        }
        public string CalculateTotal()
        {
            if (items!=null)
            {
                Money total = new Money(0, 0);
                string result= "";
                foreach (var item in items)
                {
                    total.WholePart += item.Price.WholePart * item.Quantity;
                    total.Cents += item.Price.Cents * item.Quantity;
                    result += Money.DisplayAmount(item);
                }

                total.WholePart += total.Cents / 100;
                total.Cents = total.Cents % 100;
                result += $"\n {total.WholePart}.{total.Cents} \n";
                return result;
            }
            else
            {
                return $"no item in warehouse";
            }





        }
    }
}
