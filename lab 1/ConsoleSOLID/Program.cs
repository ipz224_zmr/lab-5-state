﻿using ClassLibrarySOLID;



class Program
{

    static string Menu(){
        Console.WriteLine("1. Add item to warehouse");
        Console.WriteLine("2. Remove item from warehouse");
        Console.WriteLine("3. Register income");
        Console.WriteLine("4. Register outcome");
        Console.WriteLine("5. Generate inventory report");
        Console.WriteLine("6. Display total amount");
        Console.WriteLine("7. Reduce price");
        Console.WriteLine("8. Exit");
        Console.Write("Enter your choice: ");
        string choice = Console.ReadLine();
        return choice;
    }
    static int ReadIntegerFromConsole(string message=null)
    {
        int num;
        bool isValidInput;

        do
        {
            Console.WriteLine(message);
            string input = Console.ReadLine();
            isValidInput = int.TryParse(input, out num);
            if (!isValidInput)
            {
                Console.WriteLine("Invalid input. Please enter an integer number.");
            }
        } while (!isValidInput);

        return num;
    }
    static void Main()
    {
        var warehouseItems = new List<WarehouseItem>();
        var warehouse = new Warehouse(warehouseItems);
        var reporting = new Reporting(warehouseItems);

        int num;

        while (true)
        {
            switch (Menu())
            {
                case "1":
                    Console.WriteLine("Enter details for the new item:");
                    var newItem = WarehouseItem.GetWarehouseItemFromUserInput();
                    warehouse.AddItem(newItem);
                    break;

                case "2":
                    reporting.GenerateInventoryReport();
                    Console.Write("Enter the name of the item to remove: ");
                    string itemNameToRemove = Console.ReadLine();
                    var itemToRemove = warehouseItems.FirstOrDefault(item => item.Name == itemNameToRemove);

                    if (itemToRemove != null)
                    {
                        warehouse.RemoveItem(itemToRemove);
                        Console.WriteLine($"{itemNameToRemove} removed from the warehouse.");
                    }
                    else
                    {
                        Console.WriteLine($"Error: {itemNameToRemove} not found in the warehouse.");
                    }
                    break;

                case "3":
                    reporting.GenerateInventoryReport();
                     newItem = WarehouseItem.GetWarehouseItemFromUserInput();
                        reporting.RegisterIncome(newItem);
                    if (!warehouse.Items.Any(item => item.Equals(newItem)))
                    {
                        warehouse.AddItem(newItem);
                    }
                    break;

                case "4":
                    reporting.GenerateInventoryReport();
                    newItem = WarehouseItem.GetWarehouseItemFromUserInput();
                        reporting.RegisterOutcome(newItem);
                    if (warehouse.Items.Any(item => item.Equals(newItem)))
                    {
                        warehouse.RemoveItem(newItem);
                    }
                    break;

                case "5":
                    reporting.GenerateInventoryReport();
                    break;

                case "6":
                    Console.WriteLine("Total Price: ");
                    Console.WriteLine(warehouse.CalculateTotal());
                    break;
                case "7":
                    reporting.GenerateInventoryReport();
                    num = ReadIntegerFromConsole("Enter index item");

                    if (num > 0 && num <= warehouse.Items.Count && warehouse.Items[num - 1] != null)
                    {
                        var item = warehouse.Items[num - 1];
                        Dictionary<string, string> userInputMoney = WarehouseItem.GetUserInputMoney();
                        int wholePart = Convert.ToInt32(userInputMoney["Price (Whole Part)"]);
                        int cents = Convert.ToInt32(userInputMoney["Price (Cents)"]);
                        var money = new Money(wholePart, cents);
                        item.ReducePrice(money);
                    }
                    break;
                case "8":
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Invalid choice. Please try again.");
                    break;
            }
        }
    }
}

