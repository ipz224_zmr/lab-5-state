﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverClassLibrary
{
    public class Observer
    {
        private Dictionary<string, List<Action>> eventListeners = new Dictionary<string, List<Action>>();

        public void AddEventListener(string eventType, Action listener)
        {
            if (!eventListeners.ContainsKey(eventType))
            {
                eventListeners[eventType] = new List<Action>();
            }
            eventListeners[eventType].Add(listener);
        }

        public void TriggerEvent(string eventType)
        {
            if (eventListeners.ContainsKey(eventType))
            {
                foreach (var listener in eventListeners[eventType])
                {
                    listener();
                }
            }
        }
    }
}
