﻿using MementoClassLibrary;

class Program
{
    static void Main(string[] args)
    {
        TextEditor editor = new TextEditor();

        editor.SetContent("Hello, User!");
        editor.Save(); 
        Console.WriteLine("Current content: " + editor.GetContent());

        editor.SetContent("Goodbye, User!");
        editor.Save();
        Console.WriteLine("Current content: " + editor.GetContent());


        editor.Undo();
        Console.WriteLine("Current content after undo: " + editor.GetContent());

        editor.Undo(); 
        Console.WriteLine("Current content after undo: " + editor.GetContent());

        editor.Undo(); 
    }
}