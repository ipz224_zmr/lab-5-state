﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsClassLibrary
{
    public class FourthLevel : Handler
    {
        protected override string Report => "Перевірте наявність найновішої версії системи та драйверів";

        protected override int HelpLevel => 4;

        protected override string HandableProblem => "Пристрій часто перезавантажується";

        public override void Inquiry(UserInquiry inquiry)
        {
            if (inquiry.UserProblem.Equals(this.HandableProblem, StringComparison.CurrentCultureIgnoreCase))
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine($"Рівень {this.HelpLevel} підтримка: {this.Report}");
                Console.ResetColor();
                return;
            }

            base.Inquiry(inquiry);
        }
    }
}
